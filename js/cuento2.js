let type = 'WebGL'
    
    if(!PIXI.utils.isWebGLSupported()){
      type = 'canvas'
    }
    
	let app = new PIXI.Application({ 
	    width: 1024,         
	    height: 1024,        
	    antialias: true,    
	    transparent: false, 
	    resolution: window.devicePixelRatio || 1,	
	    backgroundColor: 0xffffff    
	  }
	);

	const loader = PIXI.Loader.shared;
	const sprites = {};
	const scenes = {};
	let curIndex = 0;
	let curIndexDialog = 0;
	let subtitle;
	let isTalking = false;
	let cCounterTime = 0;
	let tGraceTime = 1.5;
	let isGraceTime = false;
	let waitingToEnd = false;
	let sfxMute = false;
	let musicMute = false;
	let musicChannel;
	let sfxChannel;
	let voiceChannel;
	let atlas;
	let volumeSFXMusic = 0.3;

	let dialog1 = [
		{time: 0.1, quote:'Matías tiene un proyecto para la feria de ciencias de su colegio.'}, 
		{time: 4.06, quote:'¡Hará que nazcan plantitas de unos frejoles!'}		
		];

	let dialog2 = [
		{time: 0.1, quote:'Tendrá que hacerse cargo él solito: esa es la tarea que le han dejado.'}, 
		{time: 5.06, quote:'Cuando llega del colegio, corre a contarle a su abuela.'},
		{time: 8.28, quote:'Hay un error típico cuando se trabaja con plantas por primera vez: echarles demasiada agua.  Las semillas se pudren. '},
		{time: 17.1, quote:'El consejo de su abuela se parece mucho al que hizo más temprano su profesor: Acuérdense de ponerle agua a su proyecto. Pero solamente lo necesario. '}		
		];

	let dialog3 = [
		{time: 0.1, quote:'Así que Matías empieza. Busca un frasco de vidrio y dentro de él coloca algodón y algunos frejoles.'}, 
		{time: 6.78, quote:'Después: agua, apenas para humedecer el algodón. Coloca el frasco cerca de una ventana que da al patio, porque necesita luz… y ya está.'},
		{time: 16.3, quote:'A esperar hasta mañana. Cada día hay que humedecer el algodón y cada día habrá una novedad. Así le dijeron '}		
		];

	let dialog4 = [
		{time: 0.1, quote:'Pero Matías no espera. Esa misma noche, se acerca a la cocina y mete la mano en el frasco. '}, 
		{time: 6.3, quote:'Uy, el algodón está seco.'},
		{time: 8.52, quote:'Vierte un chorrito de agua y se va a dormir.'}		
		];

	let dialog5 = [
		{time: 0.1, quote:'Los frejoles ya habrán comenzado a abrirse.'}, 
		{time: 3.75, quote:'En efecto, la cubierta se ha desprendido en algunos, así que Matías echa un buen chorro de agua.'},
		{time: 10.48, quote:'Para que se abran bien.'}		
		];

	let dialog6 = [
		{time: 0.1, quote:'Al tercer día, ya debería de haber raíces.'}, 
		{time: 3.469, quote:'Es lo que señala el profesor. Pero solo se asoma una raíz chiquitita. '},
		{time: 8.6, quote:'Matías tiene miedo. Hasta las plantas se van.'},
		{time: 12.43, quote:'¿La puedo salvar, abuela?'},
		{time: 14.34, quote:'Creo que no podrás, Matías.'},
		{time: 16.72, quote:'No quiso esperar a que yo la salve. ¡Nunca más sembraré una planta!'},
		{time: 21.49, quote:'question'}
		];

	let dialog7 = [
		{time: 0.1, quote:'¿Cómo es eso de “nunca más”, Matías?'}, 
		{time: 3.06, quote:'Ni siquiera pude estar en la feria de ciencias.'},
		{time: 6.12, quote:'Pero puedes cuidar otros frejoles y tener otra plantita aquí en la casa. Tú ya sabes lo que tienes que hacer.'},
		{time: 13.5, quote:'¡No, no sé! ¡Tengo miedo! ¡La nueva plantita igual se va a ir porque todas se van!'},
		{time: 20.34, quote:'Matías toma el frasco entre sus manos. Llora mientras observa los frejoles podridos y el algodón  empapado. '},
		{time: 27.86, quote:'Y vierte más agua, mucha agua. Se despide de su proyecto y le pide perdón. '}	
		];

	let dialog8 = [
		{time: 0.6, quote:'Nada de “nunca más”, Matías. Vas a empezar de nuevo'}, 
		{time: 3.77, quote:'¡Pero la plantita se fue! Haga lo que haga, nunca va a regresar.'},
		{time: 8.11, quote:'Por eso te digo que vas a empezar de nuevo. La naturaleza es maravillosa, Mati. Todos los días se muere una plantita y todos los días nace otra. Hazla nacer. '},
		{time: 18.95, quote:'Sería como hacerla regresar, piensa Matías. No será la misma, pero será su plantita, ahora sí.'},
		{time: 25.59, quote:'Y esta vez la cuidará bien. Eso hace. Y para el noveno día ya tiene cinco tallos llenos de hojas que se escapan del frasco. '},
		{time: 33.75, quote:'Ha llegado el momento de sembrar sus plantitas en el patio. '},
		{time: 37.03, quote:'Las verá crecer, sin miedo, y sabrá que esta vez sí lo esperarán.'}			
		];

	var voice1 = new Howl({src: ['sounds/cuento2/voices/NACEPLANTITA-E1-VO.mp3'], volume:1});
	var voice2 = new Howl({src: ['sounds/cuento2/voices/NACEPLANTITA-E2-VO.mp3'], volume:1});
	var voice3 = new Howl({src: ['sounds/cuento2/voices/NACEPLANTITA-E3-VO.mp3'], volume:1});
	var voice4 = new Howl({src: ['sounds/cuento2/voices/NACEPLANTITA-E4-VO.mp3'], volume:1});
	var voice5 = new Howl({src: ['sounds/cuento2/voices/NACEPLANTITA-E5-VO.mp3'], volume:1});
	var voice6 = new Howl({src: ['sounds/cuento2/voices/NACEPLANTITA-E6-VO.mp3'], volume:1});
	var voice7 = new Howl({src: ['sounds/cuento2/voices/NACEPLANTITA-E7-VO.mp3'], volume:1});
	var voice8 = new Howl({src: ['sounds/cuento2/voices/NACEPLANTITA-E8-VO.mp3'], volume:1});
	
	var bg2s = new Howl({src: ['sounds/cuento2/musics/NACEPLANTITA-E2-MUS.mp3'], volume:volumeSFXMusic});	
	var bg5s = new Howl({src: ['sounds/cuento2/musics/NACEPLANTITA-E5-MUS.mp3'], volume:volumeSFXMusic});
	var bg6s = new Howl({src: ['sounds/cuento2/musics/NACEPLANTITA-E6-MUS.mp3'], volume:volumeSFXMusic});
	var bg7s = new Howl({src: ['sounds/cuento2/musics/NACEPLANTITA-E7-MUS.mp3'], volume:volumeSFXMusic});
	var bg8s = new Howl({src: ['sounds/cuento2/musics/NACEPLANTITA-E8-MUS.mp3'], volume:volumeSFXMusic});

	var fx3 = new Howl({src: ['sounds/cuento2/fx/NACEPLANTITA-E3-FX.mp3'], volume:volumeSFXMusic});
	var fx4 = new Howl({src: ['sounds/cuento2/fx/NACEPLANTITA-E4-FX.mp3'], volume:volumeSFXMusic});
	var fx5 = new Howl({src: ['sounds/cuento2/fx/NACEPLANTITA-E5-FX.mp3'], volume:volumeSFXMusic});

	loader
	  .add('escena1', 'images/cuento2/title.png')
	  .add('escena2', 'images/cuento2/escena1.png')
	  .add('escena3', 'images/cuento2/escena2.png')
	  .add('escena4', 'images/cuento2/escena3.png')
	  .add('escena5', 'images/cuento2/escena4.png')
	  .add('escena6', 'images/cuento2/escena5.png')
	  .add('escena7', 'images/cuento2/escena6.png')
	  .add('escena8', 'images/cuento2/escena7.png')
	  .add('escena9', 'images/cuento2/escena8.png')
	  .add('escena10', 'images/cuento2/logo.png')
	  .add('atlasjson', 'images/atlas.json');
	  	  
	loader.load((loader, resources) => {
		sprites.escena1 = new PIXI.Sprite(resources.escena1.texture);
		sprites.escena2 = new PIXI.Sprite(resources.escena2.texture);
		sprites.escena3 = new PIXI.Sprite(resources.escena3.texture);
		sprites.escena4 = new PIXI.Sprite(resources.escena4.texture);
		sprites.escena5 = new PIXI.Sprite(resources.escena5.texture);
		sprites.escena6 = new PIXI.Sprite(resources.escena6.texture);
		sprites.escena7 = new PIXI.Sprite(resources.escena7.texture);
		sprites.escena8 = new PIXI.Sprite(resources.escena8.texture);
		sprites.escena9 = new PIXI.Sprite(resources.escena9.texture);
		sprites.escena10 = new PIXI.Sprite(resources.escena10.texture);
		sprites.nextBtn = new PIXI.Sprite(resources.atlasjson.spritesheet.textures['next.png']);
		sprites.prevBtn = new PIXI.Sprite(resources.atlasjson.spritesheet.textures['back.png']);
		sprites.comenzarBtn = new PIXI.Sprite(resources.atlasjson.spritesheet.textures['comenzar.png']);
		sprites.audioBtnOn = new PIXI.Sprite(resources.atlasjson.spritesheet.textures['AUDIO.png']);
		sprites.audioBtnOff = new PIXI.Sprite(resources.atlasjson.spritesheet.textures['AUDIO-off.png']);
		sprites.musicBtnOn = new PIXI.Sprite(resources.atlasjson.spritesheet.textures['MUSICA.png']);
		sprites.musicBtnOff = new PIXI.Sprite(resources.atlasjson.spritesheet.textures['MUSICA-off.png']);
		sprites.returnBtn = new PIXI.Sprite(resources.atlasjson.spritesheet.textures['return.png']);
		sprites.pregunta1 = new PIXI.Sprite(resources.atlasjson.spritesheet.textures['pregunta1.png']);
		sprites.opciona = new PIXI.Sprite(resources.atlasjson.spritesheet.textures['opciona.png']);
		sprites.opcionb = new PIXI.Sprite(resources.atlasjson.spritesheet.textures['opcionb.png']);
	  });


  	loader.onComplete.add(() => {			
		
		initializeScenes();		
		initializeSubtitlesText();		
		initializeControlButtons();
		initializeNavigationButtons();		
		initializeQuestion();
		gotoScene(0);
		resize();
		app.ticker.add(delta => gameLoop(delta));
  	});	


  	function gameLoop(delta){

		if(isTalking){			
			cCounterTime+=1/60 * delta;

			if(!waitingToEnd){

				if(curIndexDialog==scenes[curIndex].dialog.length){												
						waitingToEnd = true
						curIndexDialog = 0;
					}else{
						if(cCounterTime>scenes[curIndex].dialog[curIndexDialog].time){

							if(scenes[curIndex].dialog[curIndexDialog].quote=="question"){
								showQuestion();
								subtitle.visible = false;
							}else{
								subtitle.text = scenes[curIndex].dialog[curIndexDialog].quote;	
								curIndexDialog++;	
							}
							
						}	
					}
				
			}else{
				if(cCounterTime > scenes[curIndex].duration){
					isTalking = false;
					isGraceTime = true;
					cCounterTime = 0;
				}
			}
			
		}else{
			if(isGraceTime){
				cCounterTime+=1/60 * delta;
				if(cCounterTime>tGraceTime){
					
					cCounterTime = 0;
					isGraceTime = 0;

					if(scenes[curIndex].canChangeAutomatic){
						doNext();
					}
				}
			}
		}
	}

	function initializeScenes(){
		scenes[0] = {n:1, p:-1, scene: sprites.escena1, canGoPrev:false, canGoNext:false, canChangeAutomatic:false, haveControls:false, onEnter:enterTitleScreen, onExit:exitTitleScreen, dialog:null, duration: 24.3, voice:null,sfx:null,bg:null};
		scenes[1] = {n:2, p:0, scene: sprites.escena2, canGoPrev:true, canGoNext:true, canChangeAutomatic:true, haveControls:true, onEnter:enterScene1, onExit:exitScene1, dialog:dialog1, duration: 6.78, voice:voice1,sfx:null,bg:null};
		scenes[2] = {n:3, p:1, scene: sprites.escena3, canGoPrev:true, canGoNext:true, canChangeAutomatic:true, haveControls:true, onEnter:enterScene1, onExit:exitScene1, dialog:dialog2, duration: 26.84, voice:voice2,sfx:null,bg:bg2s};
		scenes[3] = {n:4, p:2, scene: sprites.escena4, canGoPrev:true, canGoNext:true, canChangeAutomatic:true, haveControls:true, onEnter:enterScene1, onExit:exitScene1, dialog:dialog3, duration: 22.77, voice:voice3,sfx:fx3,bg:null};
		scenes[4] = {n:5, p:3, scene: sprites.escena5, canGoPrev:true, canGoNext:true, canChangeAutomatic:true, haveControls:true, onEnter:enterScene1, onExit:exitScene1, dialog:dialog4, duration: 11.13, voice:voice4,sfx:fx4,bg:null};
		scenes[5] = {n:6, p:4, scene: sprites.escena6, canGoPrev:true, canGoNext:true, canChangeAutomatic:true, haveControls:true, onEnter:enterScene1, onExit:exitScene1, dialog:dialog5, duration: 12.38, voice:voice5,sfx:fx5,bg:bg5s};
		scenes[6] = {n:-1, p:5, scene: sprites.escena7, canGoPrev:true, canGoNext:false, canChangeAutomatic:false, haveControls:true, onEnter:enterScene1, onExit:exitScene4, dialog:dialog6, duration: 23.22, voice:voice6,sfx:null,bg:bg6s};
		scenes[7] = {n:9, p:6, scene: sprites.escena8, canGoPrev:true, canGoNext:true, canChangeAutomatic:true, haveControls:true, onEnter:enterScene1, onExit:exitScene1, dialog:dialog7, duration: 34.72, voice:voice7,sfx:null,bg:bg7s};
		scenes[8] = {n:9, p:6, scene: sprites.escena9, canGoPrev:true, canGoNext:true, canChangeAutomatic:true, haveControls:true, onEnter:enterScene1, onExit:exitScene1, dialog:dialog8, duration: 44.5, voice:voice8,sfx:null,bg:bg8s};
		scenes[9] = {n:-1,p:1, scene: sprites.escena10, canGoPrev:true, canGoNext:false, canChangeAutomatic:true, haveControls:false, onEnter:enterLogoScreen, onExit:exitLogoScreen, dialog:null, duration: 24.3, voice:null,sfx:null,bg:null};

		for (let i = 0; i < 10; i++) {
		   scenes[i].scene.visible = false;	
		   app.stage.addChild(scenes[i].scene);
		}
	}

	function initializeQuestion(){
		sprites.opciona.scale.x = .5;
		sprites.opciona.scale.y = .5;		
		sprites.opciona.x = 150;
		sprites.opciona.y = 750;
		sprites.opciona.interactive = true;
		sprites.opciona.buttonMode = true;
		sprites.opciona.on('pointerdown', onOptionA);
		sprites.opciona.visible = false;
		app.stage.addChild(sprites.opciona);

		sprites.opcionb.scale.x = .5;
		sprites.opcionb.scale.y = .5;		
		sprites.opcionb.x = 580;
		sprites.opcionb.y = 750;
		sprites.opcionb.interactive = true;
		sprites.opcionb.buttonMode = true;
		sprites.opcionb.on('pointerdown', onOptionB);
		sprites.opcionb.visible = false;
		app.stage.addChild(sprites.opcionb);

		sprites.pregunta1.scale.x = .7;
		sprites.pregunta1.scale.y = .7;		
		sprites.pregunta1.anchor.set(0.5);
		sprites.pregunta1.x = 512;
		sprites.pregunta1.y = 620;				
		sprites.pregunta1.visible = false;
		app.stage.addChild(sprites.pregunta1);
	}

	function showQuestion(){
		sprites.opciona.visible = true;
		sprites.opcionb.visible = true;
		sprites.pregunta1.visible = true;

	}


	function hideQuestion(){
		sprites.opciona.visible = false;
		sprites.opcionb.visible = false;
		sprites.pregunta1.visible = false;

	}

	function onOptionA(){
		gotoScene(7);
	}

	function onOptionB(){
		gotoScene(8);	
	}

	function enterTitleScreen(){
		sprites.comenzarBtn.visible = true;
		subtitle.visible = false;
		
	}

	function exitTitleScreen(){	
		isTalking = false;		
		isGraceTime = false;
		sprites.comenzarBtn.visible = false;				
	}

	function enterLogoScreen(){
		isTalking = false;		
		subtitle.visible = false;
		
	}

	function exitLogoScreen(){	
		isTalking = false;		
		isGraceTime = false;			
	}


	function enterScene1(){
		isTalking = true;
		curIndexDialog = 0;
		waitingToEnd = false;
		cCounterTime = 0;
		curIndexDialog = 0;
		subtitle.text = "";		
		subtitle.visible = true;		
	}

	function exitScene1(){
		isTalking = false;		
		isGraceTime = false;	
	}


	
	function exitScene4(){
		exitScene1();
		hideQuestion();
	}



	function startStory(){
		sprites.comenzarBtn.visible = false;
		doNext();
	}

	function initializeControlButtons(){
		sprites.musicBtnOn.scale.x = .6;
		sprites.musicBtnOn.scale.y = .6;		
		sprites.musicBtnOn.x = 20;
		sprites.musicBtnOn.y = 20;
		sprites.musicBtnOn.interactive = true;
		sprites.musicBtnOn.buttonMode = true;
		sprites.musicBtnOn.on('pointerdown', toggleMusic);
		sprites.musicBtnOn.visible = true;
		app.stage.addChild(sprites.musicBtnOn);

		sprites.musicBtnOff.scale.x = .6;
		sprites.musicBtnOff.scale.y = .6;		
		sprites.musicBtnOff.x = 20;
		sprites.musicBtnOff.y = 20;
		sprites.musicBtnOff.interactive = true;
		sprites.musicBtnOff.buttonMode = true;
		sprites.musicBtnOff.on('pointerdown', toggleMusic);
		sprites.musicBtnOff.visible = false;
		app.stage.addChild(sprites.musicBtnOff);

		sprites.audioBtnOn.scale.x = .6;
		sprites.audioBtnOn.scale.y = .6;		
		sprites.audioBtnOn.x = 100;
		sprites.audioBtnOn.y = 20;
		sprites.audioBtnOn.interactive = true;
		sprites.audioBtnOn.buttonMode = true;
		sprites.audioBtnOn.on('pointerdown', toggleSFX);
		sprites.audioBtnOn.visible = true;
		app.stage.addChild(sprites.audioBtnOn);

		sprites.audioBtnOff.scale.x = .6;
		sprites.audioBtnOff.scale.y = .6;		
		sprites.audioBtnOff.x = 100;
		sprites.audioBtnOff.y = 20;
		sprites.audioBtnOff.interactive = true;
		sprites.audioBtnOff.buttonMode = true;
		sprites.audioBtnOff.on('pointerdown', toggleSFX);
		sprites.audioBtnOff.visible = false;
		app.stage.addChild(sprites.audioBtnOff);

		sprites.returnBtn.scale.x = .3;
		sprites.returnBtn.scale.y = .3;		
		sprites.returnBtn.x = 170;
		sprites.returnBtn.y = 20;
		sprites.returnBtn.interactive = true;
		sprites.returnBtn.buttonMode = true;
		sprites.returnBtn.on('pointerdown', gotoFirstScene);
		sprites.returnBtn.visible = true;
		app.stage.addChild(sprites.returnBtn);
	}

	function gotoFirstScene(){
		gotoScene(1);
	}

	function toggleSFX(){
		sfxMute = !sfxMute;
		sprites.audioBtnOn.visible = !sfxMute;
		sprites.audioBtnOff.visible = sfxMute;
		
		if(sfxMute){
			

			if(voiceChannel!=null){
				voiceChannel.volume(0);		
			}
			
		}else{
			

			if(voiceChannel!=null){
				voiceChannel.volume(1);		
			}
		}
		
	}

	function toggleMusic(){
		musicMute = !musicMute;
		sprites.musicBtnOn.visible = !musicMute;
		sprites.musicBtnOff.visible = musicMute;
		
		if(musicMute){
			if(sfxChannel!=null){
				sfxChannel.volume(0);
			}

			if(musicChannel!=null){
				musicChannel.volume(0);
			}			
		}else{

			if(sfxChannel!=null){
				sfxChannel.volume(volumeSFXMusic);
			}

			if(musicChannel!=null){
				musicChannel.volume(volumeSFXMusic);
			}			
		}
		
	}



	function checkMute(){
		if(musicMute){
			if(musicChannel!=null){
				musicChannel.volume(0);
			}	

			if(sfxChannel!=null){
				sfxChannel.volume(0);
			}

		}else{
			if(musicChannel!=null){
				musicChannel.volume(volumeSFXMusic);
			}	

			if(sfxChannel!=null){
				sfxChannel.volume(volumeSFXMusic);
			}		
		}

		if(sfxMute){
			

			if(voiceChannel!=null){
				voiceChannel.volume(0);		
			}
			
		}else{
			

			if(voiceChannel!=null){
				voiceChannel.volume(1);		
			}
		}
	}




	function initializeNavigationButtons(){
		sprites.nextBtn.scale.x = 0.3;
		sprites.nextBtn.scale.y = 0.3;		
		sprites.nextBtn.x = 850;
		sprites.nextBtn.y = 850;
		sprites.nextBtn.interactive = true;
		sprites.nextBtn.buttonMode = true;
		sprites.nextBtn.on('pointerdown', doNext);
		sprites.nextBtn.visible = false;

		sprites.prevBtn.scale.x = 0.3;
		sprites.prevBtn.scale.y = 0.3;
		sprites.prevBtn.x = 50;
		sprites.prevBtn.y = 850;
		sprites.prevBtn.interactive = true;
		sprites.prevBtn.buttonMode = true;
		sprites.prevBtn.on('pointerdown', doPrev);
		sprites.prevBtn.visible = false;

		app.stage.addChild(sprites.nextBtn);
		app.stage.addChild(sprites.prevBtn);


		sprites.comenzarBtn.scale.x = .5;
		sprites.comenzarBtn.scale.y = .5;		
		sprites.comenzarBtn.x = 700;
		sprites.comenzarBtn.y = 900;
		sprites.comenzarBtn.interactive = true;
		sprites.comenzarBtn.buttonMode = true;
		sprites.comenzarBtn.on('pointerdown', startStory);
		app.stage.addChild(sprites.comenzarBtn);


	}

	function initializeSubtitlesText(){
		const style = new PIXI.TextStyle({
		    fontFamily: 'Arial',
		    fontSize: 30,		    
		    fontWeight: 'bold',
		    fill: ['#ffffff'], // gradient
		    stroke: '#4a1850',
		    strokeThickness: 5,
		    dropShadow: true,
		    dropShadowColor: '#000000',
		    dropShadowBlur: 4,
		    dropShadowAngle: Math.PI / 6,
		    dropShadowDistance: 6,
		    wordWrap: true,
		    wordWrapWidth: 600,
		    lineJoin: 'round',
		    align: 'center'
		});

		subtitle = new PIXI.Text('', style);		
		subtitle.anchor.set(0.5);
		subtitle.x = 512;
		subtitle.y = 900;
		app.stage.addChild(subtitle);
	}


	function onExitPrevScene(prev){

		if(scenes[prev].voice!=null)
			scenes[prev].voice.stop();

		if(scenes[prev].sfx!=null)
			scenes[prev].sfx.stop();

		if(scenes[prev].bg!=null)
			scenes[prev].bg.stop();

		scenes[prev].scene.visible = false;
		scenes[prev].onExit();
	}

	function onEnterScene(indexScene){

		curIndex = indexScene;

		if(scenes[curIndex].voice!=null){			
			scenes[curIndex].voice.play();
			voiceChannel = scenes[curIndex].voice;
		}

		if(scenes[curIndex].sfx!=null){
			scenes[curIndex].sfx.play();
			sfxChannel = scenes[curIndex].sfx;
		}

		if(scenes[curIndex].bg!=null){
			scenes[curIndex].bg.play();
			musicChannel = scenes[curIndex].bg;
		}

		checkMute();

		updateNavigationButtons(curIndex);		
		updateControlButtons(curIndex);
		scenes[curIndex].scene.visible = true;
		scenes[curIndex].onEnter();		
	}

	function gotoScene(indexScene){
		onExitPrevScene(curIndex);
		onEnterScene(indexScene);		
	}

  	function doPrev(){
		
		
		onExitPrevScene(curIndex);

		if(scenes[curIndex].p!=-1)
			curIndex = scenes[curIndex].p;

		onEnterScene(curIndex);
	}


	function doNext(){
				
		onExitPrevScene(curIndex);

		if(scenes[curIndex].n!=-1)
			curIndex = scenes[curIndex].n;


		onEnterScene(curIndex);		
	}

	function updateNavigationButtons(sceneIndex){
		
		sprites.nextBtn.visible = scenes[sceneIndex].canGoNext;
		sprites.prevBtn.visible = scenes[sceneIndex].canGoPrev;

	}

	function updateControlButtons(sceneIndex){
		
		sprites.audioBtnOff.visible = scenes[sceneIndex].haveControls&&sfxMute;
		sprites.audioBtnOn.visible = scenes[sceneIndex].haveControls&&!sfxMute;
		sprites.musicBtnOff.visible = scenes[sceneIndex].haveControls&&musicMute;
		sprites.musicBtnOn.visible = scenes[sceneIndex].haveControls&&!musicMute;
		sprites.returnBtn.visible = scenes[sceneIndex].haveControls;

	}

	
	function resize() {
		
	    if (window.matchMedia("(max-width: 767px)").matches) {

			div1 = document.getElementById("container");		
			app.view.style.width = '100%';
			app.view.style.height = app.view.style.width;

		}else{

			if (window.innerWidth / window.innerHeight >= 1) {
				var w = window.innerHeight * 1;
				var h = window.innerHeight;
			} else {
				var w = window.innerWidth;
				var h = window.innerWidth / 1;
			}
			app.view.style.width = w + 'px';
			app.view.style.height = h + 'px';

		}
	}

	window.onresize = function(event) {
	    resize();
	};

	window.onload = function(event) {
	    resize();

	    const graphics = new PIXI.Graphics();
		
		const style = new PIXI.TextStyle({
		    fontFamily: 'Arial',
		    fontSize: 30,		    
		    fontWeight: 'bold',
		    fill: ['#ffffff'], 
		    stroke: '#4a1850',
		    strokeThickness: 5,
		    dropShadow: true,
		    dropShadowColor: '#000000',
		    dropShadowBlur: 4,
		    dropShadowAngle: Math.PI / 6,
		    dropShadowDistance: 6,
		    wordWrap: true,
		    wordWrapWidth: 600,
		    lineJoin: 'round',
		    align: 'center'
		});

		let loading = new PIXI.Text('Cargando...', style);		
		loading.anchor.set(0.5);
		loading.x = 512;
		loading.y = 512;
		app.stage.addChild(loading);

	};

	//Add the canvas that Pixi automatically created for you to the HTML document
	document.body.appendChild(app.view);