let type = 'WebGL'
    
    if(!PIXI.utils.isWebGLSupported()){
      type = 'canvas'
    }
    
	let app = new PIXI.Application({ 
	    width: 1024,         
	    height: 1024,        
	    antialias: true,    
	    transparent: false, 
	    resolution: window.devicePixelRatio || 1,	
	    backgroundColor: 0xffffff    
	  }
	);

	const loader = PIXI.Loader.shared;
	const sprites = {};
	const scenes = {};
	let curIndex = 0;
	let curIndexDialog = 0;
	let subtitle;
	let isTalking = false;
	let cCounterTime = 0;
	let tGraceTime = 1.5;
	let isGraceTime = false;
	let waitingToEnd = false;
	let sfxMute = false;
	let musicMute = false;
	let musicChannel;
	let sfxChannel;
	let voiceChannel;
	let atlas;
	let volumeSFXMusic = 1;


	let dialog1 = [
		{time: 1, quote:'¡Ha regresado mi abuelo!'},
		{time: 3, quote:'Esa es la primera reacción de Matías cuando ve a su tío Franco. A su tío abuelo Franco, en realidad.'},
		{time: 9.5, quote:'Tú no te acuerdas de mí, Matías. La última vez que me viste eras muy chiquito.'},
		{time: 14.5, quote:'Es el hermano menor de su abuelo, pero son tan parecidos...'},
		{time: 19, quote:'La misma nariz ancha y redondita, el mismo pelo gris, la misma sonrisa. Y Matías se siente feliz como nunca.'}
		];

	let dialog2 = [
		{time: 0, quote:'Yo soy tu tío Franco, el de los caballos. ¿Ya te acordaste?'},
		{time: 4, quote:'¡Sí! ¡Te he visto en fotos con un caballo! ¿Has venido a caballo?'},
		{time: 9, quote:'No, pero pronto traeré a una yegua.'}
		];

	let dialog3 = [
		{time: 0, quote:'Es genial el tío Franco. No solo es igualito a su abuelo. Además, es divertido.'},
		{time: 6, quote:'Van juntos al parque, juegan con la pelota, le ponen nombre a cada mariposa que ven…'},
		{time: 11, quote:'¡Y llegan hasta el río!, allí donde Matías no puede ir solo.'},
		{time: 16, quote:'Durante varios días, sus amigos Danielito y Dulce no le hacen falta para jugar.'}
		];

	let dialog4 = [
		{time: 0, quote:'Lo mejor de todo es que el tío Franco duerme en el cuarto de Matías'},
		{time: 4, quote:'y en las noches se quedan conversando hasta muy tarde, con el permiso de su mamá.'},
		{time: 9, quote:'¿Te quedarás a vivir conmigo, tío Franco?'},
		{time: 12, quote:'No, Matías. Solo bajé para hacer unos trámites. Este sábado me iré.'}
		];

	let dialog5 = [
		{time: 0, quote:'¿Te irás… al mismo lugar que mi abuelo?'},
		{time: 3, quote:'No. Yo sí voy a regresar, Matías. \n No te creo.'},
		{time: 7, quote:'question'}
		];

	let dialog6 = [
		{time: 1, quote:'Todavía se acuerda de esa noche en que el abuelo se fue al hospital y le prometieron llevarlo a visitarlo al día siguiente.'},
		{time: 8, quote:'Pero el abuelo no lo esperó.'},
		{time: 10, quote:'Tú también me estás mintiendo, tío Franco'},
		{time: 12, quote:'Grita Matías antes de salir corriendo hacia el cuarto de su mamá.'},
		{time: 16, quote:'La abuela, que ya está acostada, se despierta por el ruido.'},
		{time: 21, quote:'¿Y tú, abuela? ¿Cuándo te vas a ir?'},
		{time: 24, quote:'Lo que escucha la deja sin respuesta.'}
		];

	let dialog7 = [
		{time: 0, quote:'Mi abuelo se fue para siempre cuando tú llegaste, yo pensé que él había regresado.'},
		{time: 6, quote:'¡Y para mí, él regresó! ¡Porque tú eres igual a él! ¿Por qué me van a dejar solo otra vez?'},
		{time: 12, quote:'Ya te habrán dicho que para ver a tu abuelo solo tienes que cerrar los ojos, ¿no? \n Sí.'},
		{time: 18, quote:'¿Y funciona? ¿Puedes verlo? \n Sí.'},
		{time: 21, quote:'Conmigo será más fácil. Cada vez que quieras verme, hablaremos por videollamada.'},
		{time: 26, quote:'Es la primera vez que Matías sonríe con la carita llenecita de lágrimas. Se queda observando a su tío Franco.'},
		{time: 33, quote:'La misma nariz ancha y redondita, el mismo pelo gris, la misma sonrisa.'},
		{time: 39, quote:'Y para mí será como si nunca te hubieras ido.'},
		{time: 43, quote:'Termina diciendo Matías, ahora sin cólera ni miedo en la voz.'}
		];

	

	var voice1 = new Howl({src: ['sounds/cuento5/voices/EL TIO FRANCO-E1-VO.mp3'], volume:1});
	var voice2 = new Howl({src: ['sounds/cuento5/voices/EL TIO FRANCO-E2-VO.mp3'], volume:1});
	var voice3 = new Howl({src: ['sounds/cuento5/voices/EL TIO FRANCO-E3-VO.mp3'], volume:1});
	var voice4 = new Howl({src: ['sounds/cuento5/voices/EL TIO FRANCO-E4-VO.mp3'], volume:1});
	var voice5 = new Howl({src: ['sounds/cuento5/voices/EL TIO FRANCO-E5-VO.mp3'], volume:1});
	var voice6 = new Howl({src: ['sounds/cuento5/voices/EL TIO FRANCO-E6-VO.mp3'], volume:1});
	var voice7 = new Howl({src: ['sounds/cuento5/voices/EL TIO FRANCO-E7-VO.mp3'], volume:1});
	
	var bg1s = new Howl({src: ['sounds/cuento5/musics/EL TIO FRANCO-E1-MUS.mp3'], volume:volumeSFXMusic});	
	var bg3s = new Howl({src: ['sounds/cuento5/musics/EL TIO FRANCO-E3-MUS.mp3'], volume:volumeSFXMusic});
	var bg6s = new Howl({src: ['sounds/cuento5/musics/EL TIO FRANCO-E6-MUS.mp3'], volume:volumeSFXMusic});	
	var bg7s = new Howl({src: ['sounds/cuento5/musics/EL TIO FRANCO-E7-MUS.mp3'], volume:volumeSFXMusic});	

	var fx3 = new Howl({src: ['sounds/cuento5/fx/EL TIO FRANCO-E3-FX.mp3'], volume:volumeSFXMusic});
	var fx4 = new Howl({src: ['sounds/cuento5/fx/EL TIO FRANCO-E4-FX.mp3'], volume:volumeSFXMusic});
	var fx5 = new Howl({src: ['sounds/cuento5/fx/EL TIO FRANCO-E5-FX.mp3'], volume:volumeSFXMusic});
	var fx6 = new Howl({src: ['sounds/cuento5/fx/EL TIO FRANCO-E6-FX.mp3'], volume:volumeSFXMusic});
	
	

	loader
	  .add('escena1', 'images/cuento5/title.png')
	  .add('escena2', 'images/cuento5/escena1.png')
	  .add('escena3', 'images/cuento5/escena2.png')
	  .add('escena4', 'images/cuento5/escena3.png')
	  .add('escena5', 'images/cuento5/escena4.png')
	  .add('escena6', 'images/cuento5/escena5.png')
	  .add('escena7', 'images/cuento5/escena6.png')
	  .add('escena8', 'images/cuento5/escena7.png')
	  .add('escena10', 'images/cuento5/final.png')
	  .add('atlasjson', 'images/atlas.json');
	  	  
	loader.load((loader, resources) => {
		sprites.escena1 = new PIXI.Sprite(resources.escena1.texture);
		sprites.escena2 = new PIXI.Sprite(resources.escena2.texture);
		sprites.escena3 = new PIXI.Sprite(resources.escena3.texture);
		sprites.escena4 = new PIXI.Sprite(resources.escena4.texture);
		sprites.escena5 = new PIXI.Sprite(resources.escena5.texture);
		sprites.escena6 = new PIXI.Sprite(resources.escena6.texture);
		sprites.escena7 = new PIXI.Sprite(resources.escena7.texture);
		sprites.escena8 = new PIXI.Sprite(resources.escena8.texture);
		sprites.escena10 = new PIXI.Sprite(resources.escena10.texture);
		sprites.nextBtn = new PIXI.Sprite(resources.atlasjson.spritesheet.textures['next.png']);
		sprites.prevBtn = new PIXI.Sprite(resources.atlasjson.spritesheet.textures['back.png']);
		sprites.comenzarBtn = new PIXI.Sprite(resources.atlasjson.spritesheet.textures['comenzar.png']);
		sprites.audioBtnOn = new PIXI.Sprite(resources.atlasjson.spritesheet.textures['AUDIO.png']);
		sprites.audioBtnOff = new PIXI.Sprite(resources.atlasjson.spritesheet.textures['AUDIO-off.png']);
		sprites.musicBtnOn = new PIXI.Sprite(resources.atlasjson.spritesheet.textures['MUSICA.png']);
		sprites.musicBtnOff = new PIXI.Sprite(resources.atlasjson.spritesheet.textures['MUSICA-off.png']);
		sprites.returnBtn = new PIXI.Sprite(resources.atlasjson.spritesheet.textures['return.png']);
		sprites.pregunta1 = new PIXI.Sprite(resources.atlasjson.spritesheet.textures['pregunta1.png']);
		sprites.opciona = new PIXI.Sprite(resources.atlasjson.spritesheet.textures['opciona.png']);
		sprites.opcionb = new PIXI.Sprite(resources.atlasjson.spritesheet.textures['opcionb.png']);
	  });


  	loader.onComplete.add(() => {			
		
		initializeScenes();		
		initializeSubtitlesText();		
		initializeControlButtons();
		initializeNavigationButtons();		
		initializeQuestion();
		gotoScene(0);
		resize();
		app.ticker.add(delta => gameLoop(delta));
  	});	


  	function gameLoop(delta){

		if(isTalking){			
			cCounterTime+=1/60 * delta;

			if(!waitingToEnd){

				if(curIndexDialog==scenes[curIndex].dialog.length){												
						waitingToEnd = true
						curIndexDialog = 0;
					}else{
						if(cCounterTime>scenes[curIndex].dialog[curIndexDialog].time){

							if(scenes[curIndex].dialog[curIndexDialog].quote=="question"){
								showQuestion();
								subtitle.visible = false;
							}else{
								subtitle.text = scenes[curIndex].dialog[curIndexDialog].quote;	
								curIndexDialog++;	
							}
							
						}	
					}
				
			}else{
				if(cCounterTime > scenes[curIndex].duration){
					isTalking = false;
					isGraceTime = true;
					cCounterTime = 0;
				}
			}
			
		}else{
			if(isGraceTime){
				cCounterTime+=1/60 * delta;
				if(cCounterTime>tGraceTime){
					
					cCounterTime = 0;
					isGraceTime = 0;

					if(scenes[curIndex].canChangeAutomatic){
						doNext();
					}
				}
			}
		}
	}

	function initializeScenes(){
		scenes[0] = {n:1, p:-1, scene: sprites.escena1, canGoPrev:false, canGoNext:false, canChangeAutomatic:false, haveControls:false, onEnter:enterTitleScreen, onExit:exitTitleScreen, dialog:null, duration: 24.3, voice:null,sfx:null,bg:null};
		scenes[1] = {n:2, p:0, scene: sprites.escena2, canGoPrev:true, canGoNext:true, canChangeAutomatic:true, haveControls:true, onEnter:enterScene1, onExit:exitScene1, dialog:dialog1, duration: 29, voice:voice1,sfx:null,bg:bg1s};//16.74
		scenes[2] = {n:3, p:1, scene: sprites.escena3, canGoPrev:true, canGoNext:true, canChangeAutomatic:true, haveControls:true, onEnter:enterScene1, onExit:exitScene1, dialog:dialog2, duration: 12, voice:voice2,sfx:null,bg:null};
		scenes[3] = {n:4, p:2, scene: sprites.escena4, canGoPrev:true, canGoNext:true, canChangeAutomatic:true, haveControls:true, onEnter:enterScene1, onExit:exitScene1, dialog:dialog3, duration: 21, voice:voice3,sfx:fx3,bg:bg3s};
		scenes[4] = {n:5, p:3, scene: sprites.escena5, canGoPrev:true, canGoNext:true, canChangeAutomatic:true, haveControls:true, onEnter:enterScene1, onExit:exitScene1, dialog:dialog4, duration: 17, voice:voice4,sfx:fx4,bg:null};
		scenes[5] = {n:-1, p:4, scene: sprites.escena6, canGoPrev:true, canGoNext:false, canChangeAutomatic:false, haveControls:true, onEnter:enterScene1, onExit:exitScene4, dialog:dialog5, duration: 9, voice:voice5,sfx:fx5,bg:null};
		scenes[6] = {n:8, p:5, scene: sprites.escena7, canGoPrev:true, canGoNext:true, canChangeAutomatic:true, haveControls:true, onEnter:enterScene1, onExit:exitScene1, dialog:dialog6, duration: 28, voice:voice6,sfx:fx6,bg:bg6s};
		scenes[7] = {n:8, p:5, scene: sprites.escena8, canGoPrev:true, canGoNext:true, canChangeAutomatic:true, haveControls:true, onEnter:enterScene1, onExit:exitScene1, dialog:dialog7, duration: 51, voice:voice7,sfx:null,bg:bg7s};		
		scenes[8] = {n:-1,p:1, scene: sprites.escena10, canGoPrev:true, canGoNext:false, canChangeAutomatic:true, haveControls:false, onEnter:enterLogoScreen, onExit:exitLogoScreen, dialog:null, duration: 24.3, voice:null,sfx:null,bg:null};

		for (let i = 0; i < 9; i++) {
		   scenes[i].scene.visible = false;	
		   app.stage.addChild(scenes[i].scene);
		}
	}

	function initializeQuestion(){
		sprites.opciona.scale.x = .5;
		sprites.opciona.scale.y = .5;		
		sprites.opciona.x = 150;
		sprites.opciona.y = 750;
		sprites.opciona.interactive = true;
		sprites.opciona.buttonMode = true;
		sprites.opciona.on('pointerdown', onOptionA);
		sprites.opciona.visible = false;
		app.stage.addChild(sprites.opciona);

		sprites.opcionb.scale.x = .5;
		sprites.opcionb.scale.y = .5;		
		sprites.opcionb.x = 580;
		sprites.opcionb.y = 750;
		sprites.opcionb.interactive = true;
		sprites.opcionb.buttonMode = true;
		sprites.opcionb.on('pointerdown', onOptionB);
		sprites.opcionb.visible = false;
		app.stage.addChild(sprites.opcionb);

		sprites.pregunta1.scale.x = .7;
		sprites.pregunta1.scale.y = .7;		
		sprites.pregunta1.anchor.set(0.5);
		sprites.pregunta1.x = 512;
		sprites.pregunta1.y = 620;				
		sprites.pregunta1.visible = false;
		app.stage.addChild(sprites.pregunta1);
	}

	function showQuestion(){
		sprites.opciona.visible = true;
		sprites.opcionb.visible = true;
		sprites.pregunta1.visible = true;

	}


	function hideQuestion(){
		sprites.opciona.visible = false;
		sprites.opcionb.visible = false;
		sprites.pregunta1.visible = false;

	}

	function onOptionA(){
		gotoScene(6);
	}

	function onOptionB(){
		gotoScene(7);	
	}

	function enterTitleScreen(){
		sprites.comenzarBtn.visible = true;
		subtitle.visible = false;
		
	}

	function exitTitleScreen(){	
		isTalking = false;		
		isGraceTime = false;
		sprites.comenzarBtn.visible = false;				
	}

	function enterLogoScreen(){
		isTalking = false;		
		subtitle.visible = false;
		
	}

	function exitLogoScreen(){	
		isTalking = false;		
		isGraceTime = false;			
	}


	function enterScene1(){
		isTalking = true;
		curIndexDialog = 0;
		waitingToEnd = false;
		cCounterTime = 0;
		curIndexDialog = 0;
		subtitle.text = "";		
		subtitle.visible = true;		
	}

	function exitScene1(){
		isTalking = false;		
		isGraceTime = false;	
	}


	
	function exitScene4(){
		exitScene1();
		hideQuestion();
	}



	function startStory(){
		sprites.comenzarBtn.visible = false;
		doNext();
	}

	function initializeControlButtons(){
		sprites.musicBtnOn.scale.x = .6;
		sprites.musicBtnOn.scale.y = .6;		
		sprites.musicBtnOn.x = 20;
		sprites.musicBtnOn.y = 20;
		sprites.musicBtnOn.interactive = true;
		sprites.musicBtnOn.buttonMode = true;
		sprites.musicBtnOn.on('pointerdown', toggleMusic);
		sprites.musicBtnOn.visible = true;
		app.stage.addChild(sprites.musicBtnOn);

		sprites.musicBtnOff.scale.x = .6;
		sprites.musicBtnOff.scale.y = .6;		
		sprites.musicBtnOff.x = 20;
		sprites.musicBtnOff.y = 20;
		sprites.musicBtnOff.interactive = true;
		sprites.musicBtnOff.buttonMode = true;
		sprites.musicBtnOff.on('pointerdown', toggleMusic);
		sprites.musicBtnOff.visible = false;
		app.stage.addChild(sprites.musicBtnOff);

		sprites.audioBtnOn.scale.x = .6;
		sprites.audioBtnOn.scale.y = .6;		
		sprites.audioBtnOn.x = 100;
		sprites.audioBtnOn.y = 20;
		sprites.audioBtnOn.interactive = true;
		sprites.audioBtnOn.buttonMode = true;
		sprites.audioBtnOn.on('pointerdown', toggleSFX);
		sprites.audioBtnOn.visible = true;
		app.stage.addChild(sprites.audioBtnOn);

		sprites.audioBtnOff.scale.x = .6;
		sprites.audioBtnOff.scale.y = .6;		
		sprites.audioBtnOff.x = 100;
		sprites.audioBtnOff.y = 20;
		sprites.audioBtnOff.interactive = true;
		sprites.audioBtnOff.buttonMode = true;
		sprites.audioBtnOff.on('pointerdown', toggleSFX);
		sprites.audioBtnOff.visible = false;
		app.stage.addChild(sprites.audioBtnOff);

		sprites.returnBtn.scale.x = .3;
		sprites.returnBtn.scale.y = .3;		
		sprites.returnBtn.x = 170;
		sprites.returnBtn.y = 20;
		sprites.returnBtn.interactive = true;
		sprites.returnBtn.buttonMode = true;
		sprites.returnBtn.on('pointerdown', gotoFirstScene);
		sprites.returnBtn.visible = true;
		app.stage.addChild(sprites.returnBtn);
	}

	function gotoFirstScene(){
		gotoScene(1);
	}

		function toggleSFX(){
		sfxMute = !sfxMute;
		sprites.audioBtnOn.visible = !sfxMute;
		sprites.audioBtnOff.visible = sfxMute;
		
		if(sfxMute){
			

			if(voiceChannel!=null){
				voiceChannel.volume(0);		
			}
			
		}else{
			

			if(voiceChannel!=null){
				voiceChannel.volume(1);		
			}
		}
		
	}

	function toggleMusic(){
		musicMute = !musicMute;
		sprites.musicBtnOn.visible = !musicMute;
		sprites.musicBtnOff.visible = musicMute;
		
		if(musicMute){
			if(sfxChannel!=null){
				sfxChannel.volume(0);
			}

			if(musicChannel!=null){
				musicChannel.volume(0);
			}			
		}else{

			if(sfxChannel!=null){
				sfxChannel.volume(volumeSFXMusic);
			}

			if(musicChannel!=null){
				musicChannel.volume(volumeSFXMusic);
			}			
		}
		
	}



	function checkMute(){
		if(musicMute){
			if(musicChannel!=null){
				musicChannel.volume(0);
			}	

			if(sfxChannel!=null){
				sfxChannel.volume(0);
			}

		}else{
			if(musicChannel!=null){
				musicChannel.volume(volumeSFXMusic);
			}	

			if(sfxChannel!=null){
				sfxChannel.volume(volumeSFXMusic);
			}		
		}

		if(sfxMute){
			

			if(voiceChannel!=null){
				voiceChannel.volume(0);		
			}
			
		}else{
			

			if(voiceChannel!=null){
				voiceChannel.volume(1);		
			}
		}
	}




	function initializeNavigationButtons(){
		sprites.nextBtn.scale.x = 0.3;
		sprites.nextBtn.scale.y = 0.3;		
		sprites.nextBtn.x = 850;
		sprites.nextBtn.y = 850;
		sprites.nextBtn.interactive = true;
		sprites.nextBtn.buttonMode = true;
		sprites.nextBtn.on('pointerdown', doNext);
		sprites.nextBtn.visible = false;

		sprites.prevBtn.scale.x = 0.3;
		sprites.prevBtn.scale.y = 0.3;
		sprites.prevBtn.x = 50;
		sprites.prevBtn.y = 850;
		sprites.prevBtn.interactive = true;
		sprites.prevBtn.buttonMode = true;
		sprites.prevBtn.on('pointerdown', doPrev);
		sprites.prevBtn.visible = false;

		app.stage.addChild(sprites.nextBtn);
		app.stage.addChild(sprites.prevBtn);


		sprites.comenzarBtn.scale.x = .5;
		sprites.comenzarBtn.scale.y = .5;		
		sprites.comenzarBtn.x = 700;
		sprites.comenzarBtn.y = 900;
		sprites.comenzarBtn.interactive = true;
		sprites.comenzarBtn.buttonMode = true;
		sprites.comenzarBtn.on('pointerdown', startStory);
		app.stage.addChild(sprites.comenzarBtn);


	}

	function initializeSubtitlesText(){
		const style = new PIXI.TextStyle({
		    fontFamily: 'Arial',
		    fontSize: 30,		    
		    fontWeight: 'bold',
		    fill: ['#ffffff'], // gradient
		    stroke: '#4a1850',
		    strokeThickness: 5,
		    dropShadow: true,
		    dropShadowColor: '#000000',
		    dropShadowBlur: 4,
		    dropShadowAngle: Math.PI / 6,
		    dropShadowDistance: 6,
		    wordWrap: true,
		    wordWrapWidth: 600,
		    lineJoin: 'round',
		    align: 'center'
		});

		subtitle = new PIXI.Text('', style);		
		subtitle.anchor.set(0.5);
		subtitle.x = 512;
		subtitle.y = 900;
		app.stage.addChild(subtitle);
	}


	function onExitPrevScene(prev){

		if(scenes[prev].voice!=null)
			scenes[prev].voice.stop();

		if(scenes[prev].sfx!=null)
			scenes[prev].sfx.stop();

		if(scenes[prev].bg!=null)
			scenes[prev].bg.stop();

		scenes[prev].scene.visible = false;
		scenes[prev].onExit();
	}

	function onEnterScene(indexScene){

		curIndex = indexScene;

		if(scenes[curIndex].voice!=null){			
			scenes[curIndex].voice.play();
			voiceChannel = scenes[curIndex].voice;
		}

		if(scenes[curIndex].sfx!=null){
			scenes[curIndex].sfx.play();
			sfxChannel = scenes[curIndex].sfx;
		}

		if(scenes[curIndex].bg!=null){
			scenes[curIndex].bg.play();
			musicChannel = scenes[curIndex].bg;
		}

		checkMute();

		updateNavigationButtons(curIndex);		
		updateControlButtons(curIndex);
		scenes[curIndex].scene.visible = true;
		scenes[curIndex].onEnter();		
	}

	function gotoScene(indexScene){
		onExitPrevScene(curIndex);
		onEnterScene(indexScene);		
	}

  	function doPrev(){
		
		
		onExitPrevScene(curIndex);

		if(scenes[curIndex].p!=-1)
			curIndex = scenes[curIndex].p;

		onEnterScene(curIndex);
	}


	function doNext(){
				
		onExitPrevScene(curIndex);

		if(scenes[curIndex].n!=-1)
			curIndex = scenes[curIndex].n;


		onEnterScene(curIndex);		
	}

	function updateNavigationButtons(sceneIndex){
		
		sprites.nextBtn.visible = scenes[sceneIndex].canGoNext;
		sprites.prevBtn.visible = scenes[sceneIndex].canGoPrev;

	}

	function updateControlButtons(sceneIndex){
		
		sprites.audioBtnOff.visible = scenes[sceneIndex].haveControls&&sfxMute;
		sprites.audioBtnOn.visible = scenes[sceneIndex].haveControls&&!sfxMute;
		sprites.musicBtnOff.visible = scenes[sceneIndex].haveControls&&musicMute;
		sprites.musicBtnOn.visible = scenes[sceneIndex].haveControls&&!musicMute;
		sprites.returnBtn.visible = scenes[sceneIndex].haveControls;

	}

	
	function resize() {
		
	    if (window.matchMedia("(max-width: 767px)").matches) {

			div1 = document.getElementById("container");		
			app.view.style.width = '100%';
			app.view.style.height = app.view.style.width;

		}else{

			if (window.innerWidth / window.innerHeight >= 1) {
				var w = window.innerHeight * 1;
				var h = window.innerHeight;
			} else {
				var w = window.innerWidth;
				var h = window.innerWidth / 1;
			}
			app.view.style.width = w + 'px';
			app.view.style.height = h + 'px';

		}
	}

	window.onresize = function(event) {
	    resize();
	};

	window.onload = function(event) {
	    resize();

	    const graphics = new PIXI.Graphics();
		
		const style = new PIXI.TextStyle({
		    fontFamily: 'Arial',
		    fontSize: 30,		    
		    fontWeight: 'bold',
		    fill: ['#ffffff'], 
		    stroke: '#4a1850',
		    strokeThickness: 5,
		    dropShadow: true,
		    dropShadowColor: '#000000',
		    dropShadowBlur: 4,
		    dropShadowAngle: Math.PI / 6,
		    dropShadowDistance: 6,
		    wordWrap: true,
		    wordWrapWidth: 600,
		    lineJoin: 'round',
		    align: 'center'
		});

		let loading = new PIXI.Text('Cargando...', style);		
		loading.anchor.set(0.5);
		loading.x = 512;
		loading.y = 512;
		app.stage.addChild(loading);

	};

	//Add the canvas that Pixi automatically created for you to the HTML document
	document.body.appendChild(app.view);